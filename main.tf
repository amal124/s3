resource "aws_s3_bucket" "lseg-backend" {
  bucket="lseg-backend"
  acl="private"
  tags="${var.tags}"

  versioning{
    enabled=true
  }

  lifecycle_rule{

    enabled=true
    noncurrent_version_expiration{


      days=30
    }
  }
}
resource "aws_dynamodb_table" "dynamodb-terraform-state-lock" {
  name = "terraformstatelock"
  hash_key = "LockID"
  read_capacity = 20
  write_capacity = 20

  attribute {
    name = "LockID"
    type = "S"
  }
}


